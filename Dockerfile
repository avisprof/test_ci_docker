FROM mambaorg/micromamba

WORKDIR /app

COPY env.yml /app/env.yml
COPY [ "env.yml", "./" ]
RUN micromamba create -f env.yml
COPY [ "poetry.lock", "pyproject.toml", "./" ]
RUN micromamba run -n example poetry install --with dev 